
/**
 * Write a description of class gui here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class gui implements ITuWas {

    private Eingabefeld port, pass, secQu;
    private Taste connectBtn, saveBtn;
    private Ausgabe userCounter;
    private TextfensterFarbe tf, userList;

    private Kreis onlineImg;

    private String passStr, secpassStr;

    public gui(SERVER parent) {
        Zeichnung.setzeFenstergroesse(500, 430);
        Zeichnung.setzeScrollbar(false);
        Zeichnung.setzeTitel("Simple BlueJ Chat Server");

        new Ausgabe("Port", 10, 5, 30, 10).setzeSchriftgroesse(12);
        port = new Eingabefeld("", 10, 20, 100, 20);
        port.setzeAusgabetext("1234");
        port.setzeSchriftgroesse(12);
        port.setzeLink(parent, 10); //open connection

        connectBtn = new Taste("Start", 110, 20, 80, 20);
        connectBtn.setzeSchriftgroesse(12);
        connectBtn.setzeLink(parent, 10); // open connection

        new Ausgabe("Aktive User", 190, 5, 50, 10).setzeSchriftgroesse(8);
        userCounter = new Ausgabe("0", 190, 5, 50, 50);
        userCounter.setzeSchriftgroesse(18);

        userList = new TextfensterFarbe(10, 50, 140, 300);
        userList.setzeNurAnzeige();

        tf = new TextfensterFarbe(160, 50, 320, 300);
        tf.setzeNurAnzeige();

        new Ausgabe("IP-Adresse", 250, 5, 65, 10).setzeSchriftgroesse(12);

        new Ausgabe("Passwort", 10, 355, 60, 10).setzeSchriftgroesse(12);
        pass = new Eingabefeld("", 10, 370, 100, 20);
        pass.setzeAusgabetext("yahoo");
        pass.setzeSchriftgroesse(12);

        new Ausgabe("Geheimfrage", 120, 355, 80, 15).setzeSchriftgroesse(12);
        secQu = new Eingabefeld("", 120, 370, 100, 20);
        secQu.setzeAusgabetext("bing");
        secQu.setzeSchriftgroesse(12);

        saveBtn = new Taste("Save", 230, 370, 80, 20);
        saveBtn.setzeSchriftgroesse(12);
        saveBtn.setzeLink(this, 10);

        onlineImg = new Kreis(460, 20, 10);
        onlineImg.setzeFarbe("rot");

    }

    public void tuWas(int id) {
        if (id == 10) {
            passStr = pass.toString();
            secpassStr = secQu.toString();
        }
    }

    public void log(String s) {
        tf.println(s);
    }

    public void logError(String s) {
        tf.printlnFehler(s);
    }

    public void updateUser() {
        userList.clear();

        for (int i=0; i < chat.getUser().size(); i++) {
            String uname = (String)chat.getUser().get(i);
            if (uname.startsWith("/")) {
                uname = uname.split("/")[1];
            }

            userList.println(uname);
        }

        userCounter.setzeAusgabetext(""+chat.getUser().size());
    }

    public int getPort() {        
        return port.leseInteger(1234);
    }

    public void setHostInfo(String s) {
        Ausgabe a = new Ausgabe(s.split("/")[1], 250, 20, 200, 20);
        a.setzeSchriftgroesse(15);
        a.setzeAusrichtung(0);
    }

    public void active(boolean s) {
        if (s) {
            connectBtn.setzeAusgabetext("Stop");
            onlineImg.setzeFarbe("gruen");
        }
        else {
            connectBtn.setzeAusgabetext("Start");
            onlineImg.setzeFarbe("rot");
        }
    }

    public String[] getPass() {
        String[] out = new String[2];

        out[0] = passStr;
        out[1] = secpassStr;

        return out;
    }

}
