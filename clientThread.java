import java.net.*;
import java.io.*;

public class clientThread extends Thread{
    /** bidirektionale Schnittstelle zur Netzwerkprotokoll-Implementierung des Clients*/
    private Socket clientSocket = null;

    /** Schreibkanal zum Client*/
    private PrintWriter zumClient = null;
    /** Lesekanal vom Client*/
    private BufferedReader vomClient = null;

    private gui ui;
    private write w = null;
    private int ver = 1;
    private String username = null;
    private String passwort = "yahoo";
    private String sFrage = "bing";

    private boolean running = true;

    public clientThread(Socket cs, gui u) {
        clientSocket = cs;
        ui = u;

        String[] temp = ui.getPass();
        passwort = temp[0];
        sFrage = temp[1];
    }

    public void run() {
        try {
            zumClient = new PrintWriter(clientSocket.getOutputStream(), true);
            vomClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            chat.addUser("/login... ("+ 
                clientSocket.getRemoteSocketAddress().toString().split("/")[1].split(":")[0] 
                +")");
            ui.updateUser();
            
            zumClient.println("Hello Client");            
            zumClient.println("Gebe das Password ein...");

            // wait for passwort but answer the pings
            String in = "";
            while (!in.equals(passwort) && ver <= 3) {
                in = vomClient.readLine();

                if (in == null) {
                    close();
                    return;
                } 
                else if (in.equals("/ping")) {
                    zumClient.println("/pong");
                }

                else if (!in.equals(passwort) && ver == 1){
                    zumClient.println("wrong password ! "+ (3-ver) +" Versuche �brig");
                    ver = ver+1;
                }
                else if (!in.equals(passwort) && ver <= 2){
                    zumClient.println("wrong password !  noch "+ (3-ver) +" Versuch �brig");
                    ver = ver+1;
                }

                else if (!in.equals(passwort) && ver >= 3){
                    zumClient.println("Sicherheitsfrage: Welche Suchmaschine benutzten Sie?");
                    while (!in.equals(sFrage)) {
                        in = vomClient.readLine();

                        if (in == null) {
                            close();
                            return;
                        } 
                        else if (in.equals("/ping")) {
                            zumClient.println("/pong");
                        }
                        else if (!in.equals(sFrage)){
                            zumClient.println("Inkorrekte Sicherheitsfrage");
                            close();
                            return;
                        }
                    }
                    in = passwort;
                }
            }

            // wait for username but answer the pings
            zumClient.println("Gebe dein Username ein...");
            in = "/ping";
            while (in.equals("/ping")) {
                in = vomClient.readLine();

                if (in == null) {
                    close();
                    return;
                } 
                else if (in.equals("/ping")) {
                    zumClient.println("/pong");
                }
                else {
                    if (in.startsWith("/")) {
                        in = " " + in; // add an space to the uname if it starts with "/"
                    }
                    username = in;
                }
            }

            zumClient.println("success");
            zumClient.println(" ");            
            zumClient.println("=================");
            zumClient.println(" ");            

            ui.log("Client verbunden (" +clientSocket.getRemoteSocketAddress().toString()+")");

            chat.write(username+" joined the chat");
            
            chat.remUser("/login... ("+ 
                clientSocket.getRemoteSocketAddress().toString().split("/")[1].split(":")[0] 
                +")");
                
            chat.addUser(username +"  ("+ 
                clientSocket.getRemoteSocketAddress().toString().split("/")[1].split(":")[0] 
                +")");
                
            ui.updateUser();

            w = new write(zumClient);
            w.start();

            do {
                in = vomClient.readLine();

                if (in != null) {

                    if (in.startsWith("/")) {
                        if (in.equalsIgnoreCase("/exit")) {
                            zumClient.println("Bye !");
                            running = false;
                        }
                        else if (in.equalsIgnoreCase("/ping")) {
                            zumClient.println("/pong");
                        }
                        else if (in.equalsIgnoreCase("/clear")) {
                            zumClient.println("Chat wird gel�scht ...");
                            chat.clear();
                            w.clear();
                        }
                        else {
                            zumClient.println("unknown command !");
                        }

                    }
                    else {
                        chat.write(username+": "+in); // read and save
                    }

                }
                else {
                    break;
                }

            } while (running);

            close();

        } 
        catch(Exception e) {
        }
    }

    public void beenden() {
        running = false;

        try {
            close();
        }
        catch (IOException e) {
        }
    }

    private void close() throws IOException {
        if (!running) {
            if (username != null) {
                chat.remUser(username +"  ("+ clientSocket.getRemoteSocketAddress().toString().split("/")[1].split(":")[0] +")");
                ui.updateUser();
                chat.write(username+" is now offline ");
            }

            if (w != null) w.close();
            zumClient.close();
            vomClient.close();
            clientSocket.close();

            ui.log("Clientverbindung beendet");
        }
    }

}
