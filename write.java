import java.util.ArrayList;
import java.io.*;


/**
 * Write a description of class write here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class write extends Thread {
    private PrintWriter zumClient = null;
    private int lastReadIndex = 0;
    private boolean running = false;

    public write(PrintWriter zC) {
       zumClient = zC;
       running = true;
    }

    public void run() {
        while (running) {
            ArrayList<String> al = chat.get();

            int size = al.size();

            if (size > lastReadIndex) {

                for (int i=lastReadIndex; i < size; i++) {
                    zumClient.println(al.get(i));
                }
                lastReadIndex = size;
            }


            al = null;
            StaticTools.warte(100); // slow loop down
        }
    }

    public void close() {
        running = false;
    }
    
    public void clear() {
        lastReadIndex = 0;
    }

}
