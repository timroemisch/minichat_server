
import java.net.*;
import java.io.*;
import java.util.ArrayList;

/**
 * Erste Serverimplementierung, keine Parallelit&auml;t
 * @author ISB-Arbeitskreis, Umsetzungshilfen Informatik 12
 * @version 1.0
 */
public class SERVER implements ITuWas {

    /** bidirektionale Schnittstelle zur Netzwerkprotokoll-Implementierung des Servers*/
    private ServerSocket serverSocket = null;

    private boolean running = false;
    private gui ui;
    private ArrayList<clientThread> clients;

    /**
     * Konstruktor des Servers
     * @exception IOException eine Ausnahme tritt auf falls:<br/>
     * - der Server nicht gestartet werden kann (weil beispielsweise der Port nicht frei ist)<br/>
     * - die Clientverbindung gest&ouml;rt bzw. unterbrochen wurde.
     */
    public SERVER() throws IOException {
        ui = new gui(this);
        clients = new ArrayList<clientThread>();

        while (true) {

            while(!running) {
                StaticTools.warte(1);
            }

            ui.tuWas(10); //save passwords
            ServerStarten();
            ui.active(running);

            while (running) {
                ui.log("waiting for clients...");

                try {
                    Socket s = serverSocket.accept(); //warten auf die Verbindung

                    clientThread newClient = new clientThread(s,ui);  //open new Thread for client
                    newClient.start(); // start Thread

                    clients.add(newClient); // add new Client to list
                }
                catch (Exception e) {
                    ui.logError(""+e);
                }
            }

        }
    }

    public void tuWas(int id) {
        if (id == 10) {
            running = !running;

            if (!running) {
                for (int i=0; i<clients.size(); i++) {
                    clients.get(i).beenden();
                }

                ui.active(running);
                try {
                    ServerStoppen();
                }
                catch(Exception e) {

                }

            }
        }
    }

    /**
     * fragt den Port ab und erzeugt den Serversocket
     */
    private void ServerStarten() throws IOException {
        //Server Socket erzeugen
        serverSocket = new ServerSocket(ui.getPort());

        ui.setHostInfo(InetAddress.getLocalHost()+"");
        ui.log("Server gestartet... "+InetAddress.getLocalHost());
    }

    /**
     * schlie&szlig;t den Serversocket
     */
    private void ServerStoppen() throws IOException {
        serverSocket.close();
        ui.log("Server gestoppt...");
    }

    /**
     * Hauptprogramm zum Erzeugen des Serverobjekts
     * @param args keine Parameter beim Programmaufruf erforderlich
     */
    public static void main(String[] args) {

        try {
            new SERVER();
        } catch (Exception e) {
            System.err.println("Fehler in der Serververabeitung.");
            System.exit(1);
        }
    }
}
